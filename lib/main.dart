import 'package:cookiez/BottomNavigationLayout.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.orange,
        unselectedWidgetColor: Colors.grey,
        primaryColorDark: Colors.black87,
        primarySwatch: Colors.orange,
      ),
      home: BottomNavigationLayout(),
    );
  }
}