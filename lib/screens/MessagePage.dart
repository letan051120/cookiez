import 'package:cookiez/CarouselSlider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class MessagePage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FilterMessage(),
        Container(
          height: 10,
        ),
        Expanded(
          child: MessageList(),
        ),
      ],
    );
  }
}

class FilterMessage extends StatefulWidget {
  @override
  State<FilterMessage> createState() => _FilterMessageState();
}

class _FilterMessageState extends State<FilterMessage> {

  int modeSelected = 0;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        TextButton(
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.all(Colors.transparent)
          ),
          onPressed: (){
            setState(() {
              modeSelected = 0;
            });
          },
          child: Container(
            padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: (modeSelected!=0)
                ? Colors.black12
                : Theme.of(context).primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(50))
            ),
            child: Text(
              'Tất cả',
              style: TextStyle(
                color: (modeSelected!=0)
                  ? Colors.black54
                  : Colors.white
              ),
            ),
          )
        ),
        TextButton(
            style: ButtonStyle(
                overlayColor: MaterialStateProperty.all(Colors.transparent)
            ),
            onPressed: (){
              setState(() {
                modeSelected = 1;
              });
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: (modeSelected!=1)
                      ? Colors.black12
                      : Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.all(Radius.circular(50))
              ),
              child: Text(
                'Chưa đọc',
                style: TextStyle(
                    color: (modeSelected!=1)
                        ? Colors.black54
                        : Colors.white
                ),
              ),
            )
        ),
        TextButton(
            style: ButtonStyle(
                overlayColor: MaterialStateProperty.all(Colors.transparent)
            ),
            onPressed: (){
              setState(() {
                modeSelected = 2;
              });
            },
            child: Container(
              padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: (modeSelected!=2)
                      ? Colors.black12
                      : Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.all(Radius.circular(50))
              ),
              child: Text(
                'Đã đọc',
                style: TextStyle(
                    color: (modeSelected!=2)
                        ? Colors.black54
                        : Colors.white
                ),
              ),
            )
        ),
      ],
    );
  }
}

class MessageList extends StatelessWidget {

  List<Widget> messageList = [
    MessageNOption(),
    MessageNOption(),
    MessageNOption(),
    MessageNOption(),
    MessageNOption(),
    MessageNOption(),
    MessageNOption(),
    MessageNOption(),
  ];

  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        scrollDirection: Axis.vertical,
        children: messageList,
      ),
    );;
  }

}

class MessageNOption extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Slidable(
      child: MessageItem(),
      endActionPane: ActionPane(
        motion: DrawerMotion(),
        children: [
          SlidableButton(
            onPressed: (context){
              print('Read');
            },
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icons.check,
            iconSize: 30,
            foregroundColor: Colors.white,
          ),
          SlidableButton(
            onPressed: (context){
              print('Delete');
            },
            backgroundColor: Colors.red,
            icon: Icons.delete,
            iconSize: 30,
            foregroundColor: Colors.white,
          )
        ],
      ),
    );
  }
}




class MessageItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              blurRadius: 7,
            )
          ]
      ),
      child: Row(
        children: [
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: NetworkImage('https://images.viblo.asia/fe08fd0e-bf25-4a5a-b7e5-9dca33cfc692.png'),
                    fit: BoxFit.cover
                )
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.fromLTRB(10, 0, 16, 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Ten nguoi dung',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w800
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        '13:01',
                        style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 4,
                  ),
                  Text(
                    'Nguoi dung: Noi dung tin nhan. Noi dung tin nhan',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                height: 10,
                width: 10,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Theme.of(context).primaryColor
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

class SlidableButton extends StatelessWidget {
  SlidableButton({
    this.icon,
    this.iconSize,
    this.fontSize,
    this.label,
    this.backgroundColor,
    this.foregroundColor,
    this.borderColor,
    this.flex = 1,
    this.autoClose = true,
    required this.onPressed
  }){
    borderColor ??= backgroundColor ?? Colors.transparent;
  }

  final IconData? icon;
  final double? iconSize;
  final double? fontSize;
  final String? label;


  /// The amount of space the child's can occupy in the main axis is
  /// determined by dividing the free space according to the flex factors of the
  /// other [CustomSlidableAction]s.
  final int flex;


  final Color? backgroundColor;
  Color? borderColor;
  final Color? foregroundColor;

  /// Whether the enclosing [Slidable] will be closed after [onPressed]
  /// occurred.
  final bool autoClose;

  /// Called when the action is tapped or otherwise activated.
  /// If this callback is null, then the action will be disabled.
  final SlidableActionCallback? onPressed;


  void _handleTap(BuildContext context) {
    onPressed?.call(context);
    if (autoClose) {
      Slidable.of(context)?.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    // Gets the SlidableController.
    final controller = Slidable.of(context);

    return ValueListenableBuilder<double>(
      // Listens to the slide animation.
      valueListenable: controller!.animation,
      builder: (context, value, child) {
        // This is the maximum ratio allowed by the current action pane.
        final maxRatio = controller.actionPaneConfigurator!.extentRatio;
        final double opacity = value / maxRatio;
        return Flexible(
            flex: flex,
            fit: FlexFit.tight,
            child: Container(
                margin: EdgeInsets.fromLTRB(1, 8, 4, 8),
                child:
                ElevatedButton(
                  onPressed: () => _handleTap(context),
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
                    backgroundColor: MaterialStateProperty.all(backgroundColor),
                    foregroundColor: MaterialStateProperty.all(foregroundColor),
                    elevation: MaterialStateProperty.all(0),
                  ),
                  child: Icon(icon, size: iconSize, color: foregroundColor?.withOpacity(opacity)),
                )));
      },
    );
  }
}