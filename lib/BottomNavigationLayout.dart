import 'package:cookiez/screens/CreatePage.dart';
import 'package:cookiez/screens/HomePage.dart';
import 'package:cookiez/screens/MessagePage.dart';
import 'package:cookiez/screens/NotificationPage.dart';
import 'package:cookiez/screens/Person.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BottomNavigationLayout extends StatefulWidget{

  List<Widget> listScreens = [
    HomePage(),
    MessagePage(),
    CreatePage(),
    NotificationPage(),
    PersonalPage()
  ];

  @override
  BottomNavigationLayoutState createState() {
    return BottomNavigationLayoutState();
  }
}

class BottomNavigationLayoutState extends State<BottomNavigationLayout>{

  int currentSelectedIndex = 0;
  bool isSearch = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: Column(
            children: [
              Container(
                height: 40,
                margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: (isSearch)
                      ? Expanded(
                        child: Container(
                          padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                            color: Colors.black12
                          ),
                          child: TextField(
                            decoration: InputDecoration.collapsed(
                              hintText: (currentSelectedIndex==0)
                                  ? 'Công thức hoặc thực đơn...'
                                  : 'Nhập từ khóa cần tìm...'
                            ),
                          ),
                        ),
                      )
                      : Container(
                        child: (currentSelectedIndex==0)
                        ? Logo()
                        : Padding(
                          padding: EdgeInsets.fromLTRB(8, 0, 0, 0),
                          child: Text(
                            (currentSelectedIndex==1)
                                ?'Tin nhắn'
                                :(currentSelectedIndex==2)
                                ?'Tạo mới'
                                :(currentSelectedIndex==3)
                                ?'Thông báo'
                                :'Cá nhân',
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: (isSearch)
                      ? TextButton(
                        onPressed: (){
                          setState(() {
                            isSearch=false;
                          });
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.all(8),
                          minimumSize: Size.zero,
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                        child: Icon(
                          Icons.close_rounded,
                          color: Theme.of(context).primaryColor,
                        ),
                      )
                      : TextButton(
                        onPressed: (){
                          setState(() {
                            isSearch=true;
                          });
                        },
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.all(8),
                          minimumSize: Size.zero,
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                        child: Icon(
                          Icons.search,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: (isSearch)
                ? Container(
                  padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                  child: ListView.separated(
                    itemCount: 13,
                    itemBuilder: (context, index){
                      return Container(
                        height: 50,
                        child: Text(
                          'Hello '+index.toString(),
                          style: TextStyle(
                              fontSize: 16
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                      );
                    },
                    separatorBuilder: (context, index){
                      return Divider();
                    },
                  ),
                )
                : Container(
                  // child: SingleChildScrollView(
                    child: IndexedStack(
                      index: currentSelectedIndex,
                      children: widget.listScreens,
                    ),
                    // scrollDirection: Axis.vertical,
                  // )
                )
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: (isSearch)
      ? null
      : Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
                style: ButtonStyle(
                  overlayColor: MaterialStateProperty.all(Colors.transparent),
                ),
                onPressed: (){
                  setState(() {
                    currentSelectedIndex = 0;
                  });
                },
                child: Column(
                  children: [
                    (currentSelectedIndex!=0)
                    ? Icon( Icons.set_meal_outlined, color: Theme.of(context).unselectedWidgetColor)
                    : Icon( Icons.set_meal_rounded, color: Theme.of(context).primaryColor),
                  ],
                ),
            ),
            TextButton(
              style: ButtonStyle(
                overlayColor: MaterialStateProperty.all(Colors.transparent),
              ),
              onPressed: (){
                setState(() {
                  currentSelectedIndex = 1;
                });
              },
              child: Column(
                children: [
                  (currentSelectedIndex!=1)
                      ? Icon( Icons.chat_bubble_outline_rounded, color: Theme.of(context).unselectedWidgetColor)
                      : Icon( Icons.chat_bubble_rounded, color: Theme.of(context).primaryColor),
                ],
              ),
            ),
            TextButton(
              style: ButtonStyle(
                overlayColor: MaterialStateProperty.all(Colors.transparent),
              ),
              onPressed: (){
                setState(() {
                  currentSelectedIndex = 2;
                });
              },
              child: Column(
                children: [
                  (currentSelectedIndex!=2)
                      ? Icon( Icons.add_box_outlined, color: Theme.of(context).unselectedWidgetColor)
                      : Icon( Icons.add_box_rounded, color: Theme.of(context).primaryColor),
                ],
              ),
            ),
            TextButton(
              style: ButtonStyle(
                overlayColor: MaterialStateProperty.all(Colors.transparent),
              ),
              onPressed: (){
                setState(() {
                  currentSelectedIndex = 3;
                });
              },
              child: Column(
                children: [
                  (currentSelectedIndex!=3)
                      ? Icon( Icons.notifications_none_rounded, color: Theme.of(context).unselectedWidgetColor)
                      : Icon( Icons.notifications_rounded, color: Theme.of(context).primaryColor),
                ],
              ),
            ),
            TextButton(
              style: ButtonStyle(
                overlayColor: MaterialStateProperty.all(Colors.transparent),
              ),
              onPressed: (){
                setState(() {
                  currentSelectedIndex = 4;
                });
              },
              child: Column(
                children: [
                  (currentSelectedIndex!=4)
                      ? Icon( Icons.person_outline_rounded, color: Theme.of(context).unselectedWidgetColor)
                      : Icon( Icons.person, color: Theme.of(context).primaryColor),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}

class Logo extends StatelessWidget {
  const Logo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.zero,
          child: Image.asset('assets/imgs/logo.png'),
        ),
        Container(
          width: 10,
        ),
        Text(
          'Cookiez',
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: Colors.orange,
          ),
        ),
      ],
    );
  }
}